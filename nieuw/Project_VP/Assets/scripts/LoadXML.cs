﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine;
public class LoadXML : MonoBehaviour
{
    MedicalAppData data = new MedicalAppData();
    public Text txt;
    private void Start()
    {
        
    }
    private void Update()
    {
        
    }
    public void ReadScanerio()
    {
        string filename = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            filename = Application.dataPath + "/Scenario/" + Application.companyName + ".xml";
        }
        if (Application.platform == RuntimePlatform.Android)
        {
            filename = Application.persistentDataPath + Application.companyName + ".xml";
        }
        txt.text = filename;
        MedicalAppData.ReadFromFile(filename, out data);
        Debug.Log(MedicalAppData.WriteToFile(ref data, filename));
    }
    /*
    public GameObject textObj_1;
    public GameObject textObj_2;
    public GameObject textObj_3;

    private GameObject[] textOptions;
    private static Dictionary<string, List<string>> optionsScenes;
    public Text txtscene;
    // Start is called before the first frame update
    void Start()
    {
        textOptions = new GameObject[] { textObj_1, textObj_2, textObj_3 };
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LoadSceneData()
    {
        optionsScenes = new Dictionary<string, List<string>>();
        TextAsset xmlData = (TextAsset)Resources.Load("scenario");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xmlData.text);

        foreach (XmlNode scene in xmlDoc["scenes"].ChildNodes)
        {
            string sceneName = scene.Attributes["name"].Value;
            List<string> options = new List<string>();

            foreach (XmlNode option in scene["age"].ChildNodes)
            {
                options.Add(option.InnerText);
            }
            optionsScenes[sceneName] = options;
        }

    }
    private void PopulateText()
    {
        foreach (KeyValuePair<string, List<string>> OptionsScene in optionsScenes)
        {
            txtscene.GetComponent<Text>().text = OptionsScene.Key;

            for (int i = 0; i < textOptions.Length; i++)
            {
                textOptions[i].GetComponent<Text>().text = OptionsScene.Value[i];
            }
        }
    }
    */
}
