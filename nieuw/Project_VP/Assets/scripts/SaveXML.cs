﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine;

public class SaveXML : MonoBehaviour
{
    MedicalAppData data = new MedicalAppData();
    public Text scenario, patient, gender, medicine;
    int id = 1;

    private void Start()
    {

    }
    private void Update()
    {

    }
    public void SaveScenario()
    {
        string filename = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            filename = Application.dataPath + "/Scenario/" + Application.companyName + ".xml";
        }
        if (Application.platform == RuntimePlatform.Android)
        {
            filename = Application.persistentDataPath + Application.companyName + ".xml";
        }
      //  data.mPatients.Add();
        MedicalAppData.WriteToFile(ref data, filename);
        Debug.Log( MedicalAppData.WriteToFile(ref data, filename));
    }
   
}
